#ifndef PARTICLE_H
#define PARTICLE_H

#include <GL/glut.h>
#include <ctime>
#include <cmath>
#include <iostream>

#include <curand.h>
#include <curand_kernel.h>

#include <stdio.h>

using namespace std;

// Represents a particle of the system.
class particles
{
	public:
		particles();
		~particles();
		void cuda_free_arrays();
		
		void create_particles(int n);
		void update_partices();
		void check_collision();
		
		void allocate_in_device();
		void d_update_partices();
		
		void draw();
	
	private:
		float *mass;
		float *expire_time; // The time when the particle should be destroyed
		float *x, *y, *z; //position
		float *dx, *dy, *dz; // speed
		
		float a[3]; // forces
		float live_time;
		float prev_time;
		
		float *d_mass;
		float *d_expire_time; // The time when the particle should be destroyed
		float *d_x, *d_y, *d_z; //position
		float *d_dx, *d_dy, *d_dz; // 3 dim!
		float *d_a; // forces
		
		int n = 0;
		float dt = 0.0005;
		
		unsigned int threads, blocks;
		size_t bytes;
		unsigned int nstreams;
		cudaStream_t *stream;
		unsigned int stream_size;
};

enum {CPU, GPU};
static unsigned int target = GPU;
const static float cube_size = 2.0f;
const static float loss_energy = 2.0f;

void draw_axes(float scale);
void draw_cube();
float rnd();
float generate_lifetime(float lifetime);

#endif /** PARTICLE_H */


