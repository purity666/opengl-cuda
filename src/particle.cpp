#include "particle.h"

void draw_axes(float scale)
{
	if (scale < 1e-2)
	{
		scale = 1e-1;
	}
 
    // draw some lines
    glColor3f(1.0,0.0,0.0); // red x
    glBegin(GL_LINES);
    // x axes
 
    glVertex3f(-1.0 * scale, 0.0f, 0.0f);
    glVertex3f(1.0 * scale, 0.0f, 0.0f);
 
    glVertex3f(1.0 * scale, 0.0f, 0.0f);
    glVertex3f(0.5 * scale, 0.05f * scale, 0.0f);
 
    glVertex3f(1.0 * scale, 0.0f, 0.0f);
    glVertex3f(0.5 * scale, -0.05f * scale, 0.0f);
    glEnd();
 
    // y 
    glColor3f(0.0,1.0 * scale,0.0); // green y
    glBegin(GL_LINES);
    glVertex3f(0.0, -1.0f * scale, 0.0f);
    glVertex3f(0.0, 1.0f * scale, 0.0f);
 
    glVertex3f(0.0, 1.0f * scale, 0.0f);
    glVertex3f(0.05 * scale, 0.5f * scale, 0.0f);
 
    glVertex3f(0.0, 1.0f * scale, 0.0f);
    glVertex3f(-0.05 * scale, 0.5f * scale, 0.0f);
    glEnd();
 
    // z 
    glColor3f(0.0, 0.0, 1.0 * scale); // blue z
    glBegin(GL_LINES);
    glVertex3f(0.0, 0.0f, -1.0f * scale);
    glVertex3f(0.0, 0.0f, 1.0f * scale);
 
 
    glVertex3f(0.0, 0.0f ,1.0f * scale );
    glVertex3f(0.0, 0.05f * scale ,0.5f * scale);
 
    glVertex3f(0.0, 0.0f, 1.0f * scale);
    glVertex3f(0.0, -0.05f * scale, 0.5f * scale);
    glEnd();
 
}

void draw_cube()
{
	glColor3ub(0, 255, 255);
	glutWireCube(cube_size);
	
}

inline float rnd() // [-1;1]
{
	return static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

inline float generate_lifetime(float lifetime)
{
	return static_cast <float> (rand()) / static_cast <float>(RAND_MAX + lifetime);
}

particles::particles()
{
	srand(time(NULL));
	
	a[0] = 0.0f;
	a[1] = 0.0f;
	a[2] = -9.8f;
	
	live_time = 0.5f;
	prev_time = glutGet(GLUT_ELAPSED_TIME);
	
	glEnable( GL_POINT_SPRITE ); // GL_POINT_SPRITE_ARB if you're
                                 // using the functionality as an extension.

    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glPointSize( 5.0 );
}

particles::~particles()
{
	delete [] mass;
	if (target == CPU)
	{
		delete [] expire_time;
		delete [] x;
		delete [] y;
		delete [] z;
		delete [] dx;
		delete [] dy;
		delete [] dz;
	}
	
	if (target == GPU)
	{
		cuda_free_arrays();
		cudaFreeHost(x);
		cudaFreeHost(y);
		cudaFreeHost(z);
	}
}



void particles::create_particles(int count)
{
	if (count <= 0)
	{
		cerr << "count of points is not valid: n = " << count << endl <<
				"n must be greater than 0" << endl;
		exit(0);
	}
	
	n = count;
	mass = new float[n];
	
	if (target == CPU)
	{
		cout << "target is CPU" << endl;
		expire_time = new float[n]; // The time when the particle should be destroyed
		x = new float[n];
		y = new float[n];
		z = new float[n];
		dx = new float[n];
		dy = new float[n];
		dz = new float[n];
		
		memset(expire_time, 0, n*sizeof(float));
	}
	else
	{
		cout << "target is GPU" << endl;
		cudaMallocHost((void**)&x, n*sizeof(float));
		cudaMallocHost((void**)&y, n*sizeof(float));
		cudaMallocHost((void**)&z, n*sizeof(float));
	}

	/// initialization
	for (int i = 0; i < n; i++)
	{
		mass[i] = rnd() + 2.0f;
	}
}

void particles::update_partices()
{
	for (int i = 0; i < n; i++)
	{
		if (expire_time[i] <= 0)
		{
			x[i] = 0;
			y[i] = 0;
			z[i] = 0;
			
			dx[i] = rnd() / 2.0;
			dy[i] = rnd() / 2.0;
			dz[i] = 5.0f;
			
			expire_time[i] = generate_lifetime(live_time);
		}
		
		x[i] += dx[i]*dt;
		y[i] += dy[i]*dt;
		z[i] += dz[i]*dt;
		
		dx[i] += a[0]*dt / mass[i];
		dy[i] += a[1]*dt / mass[i];
		dz[i] += a[2]*dt / mass[i];
		
		expire_time[i] -= dt;
		
	}
	
	if (glutGet(GLUT_ELAPSED_TIME) - prev_time > 1000)
	{
		a[0] = rnd() * 100.0f;
		a[1] = rnd() * 100.0f;
		
		prev_time = glutGet(GLUT_ELAPSED_TIME);
	}
	
	check_collision();
}

void particles::check_collision()
{
	float border = cube_size / 2.0f - 0.1f;
	for (int i = 0; i < n; i++)
	{
		if (x[i] >= border || x[i] <= -border)
		{
			dx[i] *= -1.0f;
		}
		
		if (y[i] >= border || y[i] <= -border)
		{
			dy[i] /= -1.0f;
		}
		
		if (z[i] >= border)
		{
			dz[i] /= -1.0f;
		}
		if (z[i] <= -border)
		{
			dz[i] /= -loss_energy;
		}
	}
}

void particles::draw()
{
	glBegin(GL_POINTS);
	//~ glPointSize(1);
	
	for (int i = 0; i < n; i++)
	{
		float r = 0.9f;
		float g = 0.9f;
		float b = 0.9f;
		float a = z[i];
		glColor4f(r, g, b, a);	
		glVertex3f(x[i], y[i], z[i]);
	}
	glEnd();
}
