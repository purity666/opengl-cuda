#include "particle.h"

#include <cuda_profiler_api.h>

#define CudaSafeCall(error) cuda_safe_call(error, __FILE__, __LINE__)

inline void cuda_safe_call(cudaError error, const char *file, const int line)
{
	if (error != cudaSuccess) 
	{
		cerr << "cuda error " << file <<  ": " << line << " " << cudaGetErrorString(error) << endl;
		exit(-1);
	}
}

__global__ void kernel_update_a(float *a)
{
	curandState s;
    curand_init(clock64() + 1000, 0, 0, &s);
    
    a[0] = (curand_uniform(&s) - curand_uniform(&s)) * 500.0f;
	a[1] = (curand_uniform(&s) - curand_uniform(&s)) * 500.0f;
}

__global__ void kernel_update_prtcls
										(	float *x, float *y, float *z,
											float *dx, float *dy, float *dz,
											float *mass, float *a, float *expire_time,
											float dt, float step, float border,
											int n
										 )
{
	unsigned int i = threadIdx.x + blockDim.x*blockIdx.x;
	
	if (i >= n)
	{
		return;
	}
	
	curandState s;
    curand_init(clock64() + 1000 * i, 0, 0, &s);
    
	if (expire_time[i] <= 0)
	{	
		x[i] = 0;
		y[i] = 0;
		z[i] = 0;
		
		dx[i] = (curand_uniform(&s) - curand_uniform(&s)) / 2.0f;
		dy[i] = (curand_uniform(&s) - curand_uniform(&s)) / 2.0f;
		dz[i] = 5.0f;
		
		expire_time[i]  = curand_normal(&s);
	}
	
	if (x[i] >= border || x[i] <= -border)
	{
		dx[i] /= -5.0f;
	}
	
	if (y[i] >= border || y[i] <= -border)
	{
		dy[i] /= -5.0f;
	}
	
	if (z[i] >= border || z[i] <= -border)
	{
		dx[i] /= -loss_energy;
		dy[i] /= -loss_energy;
		dz[i] /= -loss_energy;
	}
	
	x[i] += dx[i]*dt;
	y[i] += dy[i]*dt;
	z[i] += dz[i]*dt;
	
	dx[i] += a[0]*dt / mass[i];
	dy[i] += a[1]*dt / mass[i];
	dz[i] += a[2]*dt / mass[i];
	
	expire_time[i] -= step;
}

void particles::allocate_in_device()
{
	bytes = n * sizeof(float);
	
	cudaMalloc((void**)& d_mass, bytes);
	cudaMalloc((void**)& d_expire_time, bytes);
	
	cudaMalloc((void**)& d_x, bytes);
	cudaMalloc((void**)& d_y, bytes);
	cudaMalloc((void**)& d_z, bytes);
	
	cudaMalloc((void**)& d_dx, bytes);
	cudaMalloc((void**)& d_dy, bytes);
	cudaMalloc((void**)& d_dz, bytes);
	
	cudaMalloc((void**)& d_a, 3 * sizeof(float));
	
	cudaMemcpyAsync(d_mass, mass, bytes, cudaMemcpyHostToDevice);
	
	threads = n < 1024/4 ? n : 1024/4;
	blocks = (n + threads - 1) / threads;
	nstreams = 16;
	stream = new cudaStream_t[nstreams];
	for (auto s = 0; s < nstreams; s++)
	{
		cudaStreamCreateWithFlags(&stream[s], cudaStreamNonBlocking);
	}
	stream_size = n / nstreams;
}

void particles::d_update_partices()
{
	if (glutGet(GLUT_ELAPSED_TIME) - prev_time > 100)
	{
		kernel_update_a<<<1,1, 0, stream[rand() % nstreams]>>>(d_a);
		prev_time = glutGet(GLUT_ELAPSED_TIME);
	}
	
	unsigned int offset;
	
	for (unsigned int s = 0; s < nstreams; s++)
	{
		offset = s*stream_size;
			
		kernel_update_prtcls<<<stream_size / blocks, threads, 0, stream[s]>>> (
													d_x + offset, d_y + offset, d_z + offset,
													d_dx + offset, d_dy + offset, d_dz + offset,
													d_mass + offset, d_a, d_expire_time + offset,
													dt,	3.0f*dt, cube_size / 2.0f,
													stream_size
												);
	}
	
	for (unsigned int s = 0; s < nstreams; s++)
	{
		offset = s*stream_size;
		cudaMemcpyAsync(&x[offset], &d_x[offset], bytes / nstreams, cudaMemcpyDeviceToHost, stream[s]);
	}
	for (unsigned int s = 0; s < nstreams; s++)
	{
		offset = s*stream_size;
		cudaMemcpyAsync(&y[offset], &d_y[offset], bytes / nstreams, cudaMemcpyDeviceToHost, stream[s]);
	}
	for (unsigned int s = 0; s < nstreams; s++)
	{
		offset = s*stream_size;
		cudaMemcpyAsync(&z[offset], &d_z[offset], bytes / nstreams, cudaMemcpyDeviceToHost, stream[s]);
	}

	cudaDeviceSynchronize();
}

void particles::cuda_free_arrays()
{
	cudaFree(d_mass);
	cudaFree(d_expire_time);
	cudaFree(d_x);
	cudaFree(d_y);
	cudaFree(d_z);
	cudaFree(d_dx);
	cudaFree(d_dy);
	cudaFree(d_dz);
	cudaFree(d_a);
	for (auto s = 0; s < nstreams; s++)
	{
		cudaStreamDestroy(stream[s]);
	}
	delete [] stream;
}
