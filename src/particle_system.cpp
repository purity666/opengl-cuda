#include <GL/glew.h>
#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#include <GL/freeglut.h>
#endif

#include <cstdio>
#include <cmath>

#include "particle.h"

particles *p_system;

// Camera parameters
int ox, oy;
int buttonState = 0;
float camera_trans[] = { 0, 0, -3 };
float camera_rot[] = { -90, 0, 0 };
float camera_trans_lag[] = { 0, 0, -3 };
float camera_rot_lag[] = { 0, 0, 0 };
const float inertia = 0.1f;

// Window dimensions
int windowWidth = 2*640;
int windowHeight = 2*640;

// FPS info
int frame = 0;

/**
 * Calculate and display the current frame rate for benchmarking
 */
void calculateFrameRate() 
{
	static float framesPerSecond = 0.0f; // This will store our fps
	static float lastTime = 0.0f; // This will hold the time from the last frame
	float currentTime = glutGet(GLUT_ELAPSED_TIME);
	framesPerSecond++;
	if (currentTime - lastTime > 1000.0f) 
	{
		lastTime = currentTime;
		char fps[10];
		sprintf(fps, "FPS: %d",
				(int) framesPerSecond);
		glutSetWindowTitle(fps);
		framesPerSecond = 0;
	}
}

/**
 * Update particle system here. This is called by the GLUT
 * loop when nothing else is happening.
 */
static void idle(void) 
{
	calculateFrameRate();

	if (target == CPU)
	{
		p_system->update_partices();
	}
	else if (target == GPU)
	{
		p_system->d_update_partices();
	}

	glutPostRedisplay();
}

/**
 * Renders to the screen.
 */
static void render(void) 
{
	// Clear the screen to begin with
	//glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Move the camera
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Update for inertia
	for (int i = 0; i < 3; i++) 
	{
		camera_trans_lag[i] += (camera_trans[i] - camera_trans_lag[i]) * inertia;
		camera_rot_lag[i] += (camera_rot[i] - camera_rot_lag[i]) * inertia;
	}

	// Perform translation and rotation based on view parameters
	glTranslatef(camera_trans_lag[0], camera_trans_lag[1], camera_trans_lag[2]);
	glRotatef(camera_rot_lag[0], 1.0, 0.0, 0.0);
	glRotatef(camera_rot_lag[1], 0.0, 1.0, 0.0);

	// Draw the particles
	p_system->draw();

	// Draw the coordinate lines
	draw_axes(1);
	
	draw_cube();

	// Render to the screen
	glutSwapBuffers();
}

/**
 * Called when the window resizes. Reload the camera projection,
 * updating for the new aspect ratio.
 */
void reshape(int w, int h) 
{
	float fov = 60.0f;
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fov, (float) w / (float) h, 0.1, 100.0);

	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, w, h);
}

/**
 * Called when the mouse changes state.
 */
void mouse(int button, int state, int x, int y) 
{
	int mods;

	if (state == GLUT_DOWN) 
	{
		buttonState |= 1 << button;
	} 
	else if (state == GLUT_UP) 
	{
		buttonState = 0;
	}

	mods = glutGetModifiers();

	if (mods & GLUT_ACTIVE_SHIFT) 
	{
		buttonState = 2;
	} 
	else if (mods & GLUT_ACTIVE_CTRL) 
	{
		buttonState = 3;
	}

	ox = x;
	oy = y;

	glutPostRedisplay();
}

/**
 * Called when the mouse is dragged. Updates the camera
 * parameters depending on the button state.
 */
void motion(int x, int y) 
{
	float dx, dy;
	dx = (float) (x - ox);
	dy = (float) (y - oy);

	if (buttonState == 3 ) 
	{
		// left+middle = zoom
		camera_trans[2] += (dy / 100.0f) * 0.5f * fabs(camera_trans[2]);
	} 
	else if (buttonState & 2) 
	{
		// middle = translate
		camera_trans[0] += dx / 100.0f;
		camera_trans[1] -= dy / 100.0f;
	} 
	else if (buttonState & 1) 
	{
		// left = rotate
		camera_rot[0] += dy / 5.0f;
		camera_rot[1] += dx / 5.0f;
	}

	ox = x;
	oy = y;

	glutPostRedisplay();
}

void initGL(int argc, char **argv) 
{
	glutInit(&argc, argv);
	glutInitDisplayMode(
			GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("Particle System");
	glutIdleFunc(&idle);
	glutDisplayFunc(&render);
	glutReshapeFunc(&reshape);
	glutMotionFunc(&motion);
	glutMouseFunc(&mouse);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glutReportErrors();
	
}

int main(int argc, char **argv) 
{
	initGL(argc, argv);
	
	p_system = new particles();
	
	// Default number of particles
	int numParticles = 100000;
	
	// Command line argument used to decide how many particles
	if (argc > 1) 
	{
		numParticles = atoi(argv[1]);
		
		// If they haven't entered a number
		if (numParticles == 0) 
		{
			cerr << "Please enter a valid number of particles (1-10000)" << endl;
			return 1;
		}
		
		//~ if (argc > 2 && !strcmp(argv[2], "GPU"))
		//~ {
			//~ target = GPU;
		//~ }
		//~ else
		//~ {
			//~ target = CPU;
		//~ }
	}
	
	p_system->create_particles(numParticles);
	if (target == GPU)
	{
		p_system->allocate_in_device();
	}

	glutMainLoop();

	delete(p_system);

	return 0;
}
